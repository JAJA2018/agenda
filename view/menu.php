<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="https://www.google.com.br">Agenda</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Operaçoes <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="http://localhost/agenda/view/cadastrar.php">
            <i class="fa fa-plus" aria-hidden="true"></i>Cadastrar</a></li>
            <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i>Cunsultar</a></li>
            <li><a href="#"> <i class="fa fa-pencil" aria-hidden="true"></i>Alterar</a></li>
            
            <li><a href="#"><i class="fa fa-trash" aria-hidden="true"></i>Excluir</a></li>
            <li role="separator" class="divider"></li>
            <i class="fa fa-sign-out" aria-hidden="true"></i>log-out</a></li>
          </ul>
        </li>
      </ul>
   
      
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
