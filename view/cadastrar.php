<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agenda Telefônica</title>
    
<?php
include('../css/bootstrap.php');
?>
   
</head>

<body>
<?php
include('../view/menu.php');
?>

    <div class="panel panel-default">
    <div class="panel-heading">

    </div>
  <div class="panel-body">
  <form action="">
    <div class="form-group col-md-12 col-xs-12">
        <label for ="nome">Nome</label><span class="text-red">*</span>
        <input type ="text" name="nome" id="nome" required class ="form-control">

  </div>

  <div class="form-group col-md-4 col-xs-4">
        <label for ="foneres">Fone Residencial</label><span class="text-red">*</span>
        <input type ="text" name="foneres" id="foneres" required class ="form-control">
  </div>

  <div class="form-group col-md-4 col-xs-4">
        <label for ="foneres">Fone celular</label><span class="text-red">*</span>
        <input type ="text" name="foneres" id="foneres" required class ="form-control">
  </div>


  <div class="form-group col-md-12 col-xs-12">
        <label for ="e-mail">e-mail</label><span class="text-red">*</span>
        <input type ="text" name="nome" id="e-mail" required class ="form-control">


  </div>

  <div class="form-group col-md-3 col-xs-3">
        <label for ="dtnas">Data de Nascimento</label><span class="text-red">*</span>
        <input type ="text" name="dtnas" id="dtnas" required class ="form-control">
  </div>
        
  <div class="form-group col-md-12 col-xs-12">
        <label for ="genero"><i class="fa fa-venus-mars" aria-hidden="true">
        </i>Genero</label><span class="text-red">*</span>
        <select name="genero" id="genero" required class ="form-control">
          <option value= "M">Masculino</option>
          <option value= "F">Feminino</option>
          <option value= "O">Outros</option>
        </select>
  </div>

  
 </form>
    </div>
    <div class="panel-footer">Panel footer</div>

    <div class= "form-group col-md-12 col-xs-12">
  
  <button type="submit" class="btn btn-defalt">
  <i class="fa fa-undo" aria-hidden="true"></i> Voltar</button>
  <button type="submit" class="btn btn-success ">
  <i class="fa fa-floppy-o" aria-hidden="true"></i> Gravar</button>
</div>
    </div>
  



  <?php
include('../js/bootstrap.php');
?>

</body>
</html> 